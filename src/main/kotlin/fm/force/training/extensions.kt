package fm.force.training

fun Int.pow2() = this * this
