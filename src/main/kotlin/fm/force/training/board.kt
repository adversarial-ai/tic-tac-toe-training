package fm.force.training

import kotlin.math.pow
import kotlin.reflect.full.primaryConstructor

enum class Cell {
    P1,
    P2,
    EMPTY;

    val isEmpty: Boolean by lazy { this == EMPTY }
}

typealias BoardState = Array<Cell>
typealias PointCell = Pair<Point, Cell>

data class Point(var row: Int, var col: Int)   // 0-based

data class Board(
        val width: Int,
        val strokeToWin: Int,
        val state: BoardState,
) {
    init {
        require(strokeToWin in 3..width)
        require(state.count() == width.pow2())
    }

    constructor(width: Int, strokeToWin: Int) : this(width, strokeToWin, Array(width.pow2()) { Cell.EMPTY })

    private val movesCount = state.count { !it.isEmpty }
    val isFull = movesCount == state.count()
    val player1Turn = movesCount % 2 == 0

    /**
     Convert boardState (array of tertiary numbers) into decimal.
     Other fields (width etc) can be neglected because we will compare boards with same characteristics
     */
    override fun hashCode(): Int {
        var result = 0
        for ((at, value) in state.withIndex())
            result += value.ordinal * 3.toDouble().pow(at).toInt()
        return result
    }

    override fun equals(other: Any?): Boolean {
        if (other is Board)
            return state.contentEquals(other.state)
        return false
    }

    private fun toPoint(at: Int): Point {
        val col = at % width
        val row = at / width
        return Point(row, col)
    }

    private fun to1D(at: Point): Int = at.row * width + at.col
    fun cellAt(at: Point): Cell = state[to1D(at)]

    private fun strokeLength(cell: Cell, iterator: AbstractBoardIterator): Int {
        var len = 0
        for ((_, itCell) in iterator) {
            when (itCell) {
                cell -> len++
                else -> return len
            }
        }
        return len
    }

    fun isWinningStroke(at: Int): Boolean {
        val point = toPoint(at)
        val cell = state[at]
        for ((It1, It2) in boardAdjacentIterators.iterator()) {
            val iter1 = It1.primaryConstructor!!.call(this, point)
            val iter2 = It2.primaryConstructor!!.call(this, point)
            if (strokeLength(cell, iter1) + strokeLength(cell, iter2) + 1 >= strokeToWin) {
                return true
            }
        }
        return false
    }

    fun move(at: Int): Board {
        require(state[at].isEmpty) { "Cell is not empty" }
        val cellKey = if (player1Turn) Cell.P1 else Cell.P2
        val newState = state.copyOf().also { it[at] = cellKey }
        return copy(state = newState)
    }

    fun allNextMoves(): List<Pair<Board, Boolean>> {
        val results = mutableListOf<Pair<Board, Boolean>>()
        for ((at, value) in state.withIndex())
            if (value.isEmpty) {
                val newBoard = move(at)
                val isWinning = newBoard.isWinningStroke(at)
                results.add(Pair(newBoard, isWinning))
            }
        return results
    }
}

open class AbstractBoardIterator(
        private val board: Board,
        at: Point,
        val hasNext: (Point) -> Boolean,
        val step: (Point) -> Unit
) : Iterator<PointCell> {
    private var cursor = at.copy()

    override operator fun hasNext(): Boolean = hasNext(cursor)

    override operator fun next(): PointCell {
        step(cursor)
        return Pair(cursor, board.cellAt(cursor))
    }
}

class LeftIterator(board: Board, at: Point) : AbstractBoardIterator(board, at, hasNext = { it.col > 0 }, step = { it.col-- })
class RightIterator(board: Board, at: Point) : AbstractBoardIterator(board, at, hasNext = { it.col < board.width - 1 }, step = { it.col++ })
class TopIterator(board: Board, at: Point) : AbstractBoardIterator(board, at, hasNext = { it.row > 0 }, step = { it.row-- })
class BottomIterator(board: Board, at: Point) : AbstractBoardIterator(board, at, hasNext = { it.row < board.width - 1 }, step = { it.row++ })
class TopLeftIterator(board: Board, at: Point) : AbstractBoardIterator(board, at, hasNext = { it.row > 0 && it.col > 0 }, step = { it.row--; it.col-- })
class TopRightIterator(board: Board, at: Point) : AbstractBoardIterator(board, at, hasNext = { it.col < board.width - 1 && it.row > 0 }, step = { it.row--; it.col++ })
class BottomLeftIterator(board: Board, at: Point) : AbstractBoardIterator(board, at, hasNext = { it.row < board.width - 1 && it.col > 0 }, step = { it.row++; it.col-- })
class BottomRightIterator(board: Board, at: Point) : AbstractBoardIterator(board, at, hasNext = { it.row < board.width - 1 && it.col < board.width - 1 }, step = { it.row++; it.col++ })

private val boardAdjacentIterators = listOf(
        Pair(LeftIterator::class, RightIterator::class),
        Pair(TopIterator::class, BottomIterator::class),
        Pair(TopLeftIterator::class, BottomRightIterator::class),
        Pair(TopRightIterator::class, BottomLeftIterator::class),
)


class BoardGraph(
        val width: Int,
        strokeToWin: Int,
) {
    val initialNode = Board(width, strokeToWin)
    val nodes: HashSet<Board> by lazy { populate() }    // all found states

    init {
        require(strokeToWin in 3..width)
    }

    private fun populate(): HashSet<Board> {
        val nodes = HashSet<Board>()
        val pendingParents = HashSet<Board>()     // parents without children yet
        var iterations = 0
        var lastNodesSize = 0
        var lastPendingParentsSize = 0

        println("initialize ${width}x${width} combinations...")
        nodes.add(initialNode)
        pendingParents.add(initialNode)

        while (pendingParents.size > 0) {
            proceedPendingParent(nodes, pendingParents)
            iterations++

            if (iterations % 100000 == 0) {
                println("  progress: +${nodes.size - lastNodesSize} nodes (${nodes.size}), ${pendingParents.size - lastPendingParentsSize} pending parent nodes")
                lastNodesSize = nodes.size
                lastPendingParentsSize = pendingParents.size
            }
        }
        println("initialized ${nodes.size} combinations")
        return nodes
    }

    private fun proceedPendingParent(nodes: HashSet<Board>, pendingParents: HashSet<Board>) {
        val parent = pendingParents.first()
        pendingParents.remove(parent)
        for ((child, isWinning) in parent.allNextMoves()) {
            if (!(isWinning || child.isFull)) {
                nodes.add(child)
                pendingParents.add(child)
            }
        }
    }
}
