package fm.force.training

import com.beust.jcommander.*
import java.io.File
import java.time.LocalDateTime.now

class Args {
    @Parameter(names=["-w", "--width"], description="Board width (default=3)")
    var boardWidth: Int = 3

    @Parameter(names=["-s", "--stroke"], description="Stroke to win (default=3)")
    var strokeToWin: Int = 3

    @Parameter(names=["-g", "--games"], description="Training iterations (default=10000)")
    var gamesCount: Int = 10000

    @Parameter(names=["-p1", "--player1-export"], description="Path to export player 1 training results (JSON)")
    var player1ExportPath: String? = null

    @Parameter(names=["-p2", "--player2-export"], description="Path to export player 2 training results (JSON)")
    var player2ExportPath: String? = null
}


fun main(args: Array<String>) {
    val params = Args()
    JCommander.newBuilder().addObject(params).build().parse(*args)

    val graph = BoardGraph(params.boardWidth, params.strokeToWin)
    val player1 = AI(graph, true)
    val player2 = AI(graph, false)

    // continue training if result file exists
    listOf(
        Pair(player1, params.player1ExportPath),
        Pair(player2, params.player2ExportPath),
    )
        .forEach { (player, path) ->
            if (path != null && File(path).exists())
                player.loadTrainingResults(path)
            else
                player.populate()
    }

    println("play ${params.gamesCount} games...")

    for (i in 1..params.gamesCount) {
        if (i % 10000 == 0)
            println("${now()}: progress: ${i.toFloat() / params.gamesCount * 100}%")

            // dump intermediate results
            params.player1ExportPath?.let { player1.dumpTrainingResults(it) }
            params.player2ExportPath?.let { player2.dumpTrainingResults(it) }

        val game = Game(graph, player1, player2)
        player1.train(game)
        player2.train(game)
    }

    params.player1ExportPath?.let { player1.dumpTrainingResults(it) }
    params.player2ExportPath?.let { player2.dumpTrainingResults(it) }
}
