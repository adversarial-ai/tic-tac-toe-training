package fm.force.training

data class Game(
        val boardGraph: BoardGraph,
        val player1: AI,
        val player2: AI,
) {
    // alternate coordinates of each player's moves
    val moves: List<Int> by lazy { play() }
    // same count as moves (excluding initial board)
    val boards: List<Board> by lazy { calcBoards() }
    val isDraw: Boolean by lazy { !boards.last().isWinningStroke(moves.last()) }
    val player1Won by lazy { !isDraw && !boards.last().player1Turn }
    val player2Won by lazy { !isDraw && !player1Won }

    private fun play(): List<Int> {
        var board = boardGraph.initialNode
        val moves = mutableListOf<Int>()

        do {
            val nextPlayer = if (board.player1Turn) player1 else player2
            val move = nextPlayer.preferredMove(board)
            board = board.move(move)
            moves.add(move)
        } while (!(board.isWinningStroke(move) || board.isFull))

        return moves
    }

    private fun calcBoards(): List<Board> {
        val boards = mutableListOf<Board>()
        var board = boardGraph.initialNode

        for (move in moves) {
            board = board.move(move)
            boards.add(board)
        }
        return boards
    }

}
