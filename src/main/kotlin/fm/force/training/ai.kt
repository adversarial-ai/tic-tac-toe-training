package fm.force.training

import java.io.File

typealias BoardWeights = Array<Int>
typealias BoardWeightsMap = HashMap<Int, BoardWeights>

data class TrainingModel(
    val initialCellWeight: Int = 10,
    val losingWeightDelta: Int = -1,
    val winningWeightDelta: Int = 2,
    val drawWeightDelta: Int = 1,
    val maxWeight: Int = 100
)

data class AI(
    val boardGraph: BoardGraph,
    val player1: Boolean,
    val model: TrainingModel = TrainingModel(),
) {
    private val weightsMap = BoardWeightsMap()
    private val playerNum = if (player1) 1 else 2

    private fun checkInitialized() = require(weightsMap.size > 0) { "Training initialization required (with blank or pre-populated data)" }

    /**
    create blank (equally weighted) coefficients for training for every appropriate board
     */
    fun populate() {
        println("initialize player$playerNum...")
        weightsMap.clear()
        val sizeSquare = boardGraph.width.pow2()
        for (board in boardGraph.nodes)
            if (!board.isFull && board.player1Turn == player1)
                // non-empty cells have zero weight
                weightsMap[board.hashCode()] = Array(sizeSquare) { if (board.state[it].isEmpty) model.initialCellWeight else 0 }
        println("initialize player$playerNum... ${weightsMap.size} nodes")
    }

    @Suppress("unused")
    fun hasMove(board: Board): Boolean = weightsMap.containsKey(board.hashCode())

    /**
     * pick random cell position for move (according to trained weights) if board is not full
     */
    fun preferredMove(board: Board): Int {
        checkInitialized()

        val weights = weightsMap.getValue(board.hashCode())
        val indexes = mutableListOf<Int>()

        for ((index, weight) in weights.withIndex())
            if (weight > 0)
                indexes += List(weight) { index }

        return indexes.random()
    }

    /**
     * update cell coefficients of each player's cell involved in game. Delta depends on whether game was win/lose/draw.
     */
    fun train(game: Game) {
        checkInitialized()

        val delta =  if ((game.player1Won && player1) || (game.player2Won && !player1)) {
            model.winningWeightDelta
        } else if (game.isDraw) {
            model.drawWeightDelta
        } else {
            model.losingWeightDelta
        }

        var prevBoard = game.boardGraph.initialNode

        for ((move, board) in game.moves zip game.boards) {
            // filter boards with right player's turn
            if (prevBoard.player1Turn == player1) {
                val weights = weightsMap.getValue(prevBoard.hashCode())
                // apply cell weight increment. Keep it in range 1..maxWeight
                val newWeight = weights[move] + delta
                weights[move] = when {
                    newWeight < 1 -> 1   // (move can't be disallowed completely)
                    newWeight > model.maxWeight -> model.maxWeight
                    else -> newWeight
                }
            }
            prevBoard = board
        }
    }

    fun dumpTrainingResults(path: String) {
        checkInitialized()
        println("Dump Player $playerNum training data to ${path}...")

        val file = File(path)
        file.parentFile.mkdirs()
        file.bufferedWriter().use {
            it.write("{\n")
            weightsMap.forEach { (hash, array) ->
                it.write("    \"${hash}\": ${array.joinToString(prefix = "[", postfix = "]")},\n")
            }
            it.write("}")
        }
    }

    fun loadTrainingResults(path: String) {
        weightsMap.clear()
        println("Load Player $playerNum training data from ${path}...")

        val boardSize = boardGraph.width.pow2()
        val file = File(path)
        file.bufferedReader().forEachLine { line ->
            line
                .trim('{', '}', ',', ' ', '\n')
                .takeUnless { it.isEmpty() }
                ?.split(": ")
                ?.let {
                    Pair(
                        it[0].trim('\"').toInt(),
                        it[1].trim('[', ']').split(", ").map { e -> e.toInt() }.toTypedArray()
                    )
                }
                ?.also {
                    require(it.second.size == boardSize) { "model board width mismatch. Required: $boardSize, got: ${it.second.size}" }
                }
                ?.let {
                    weightsMap[it.first] = it.second
                }
        }

        println("Loaded ${weightsMap.size} training rows")
    }
}
